﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    [SerializeField]
    Transform spawn;

    void Start ()
    {
		
	}
	
	void Update ()
    {

	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Car")
        {
            other.transform.position = spawn.position;
            other.GetComponent<Renderer>().material.SetColor("_Color", Random.ColorHSV());
        }
        if (other.tag == "Train")
        {
            other.transform.position = spawn.position;
        }
    }
}
