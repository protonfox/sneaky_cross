﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaitToStart : MonoBehaviour
{
    [SerializeField]
    float waitXSeconds;

    [SerializeField]
    Text waitValue;  // Timer

    [SerializeField]
    GameObject waitText;

    float timer;
    bool waiting;

	void Start ()
    {
        waiting = true;
        timer = waitXSeconds;
        //find any active canvases, turn off.
    }

    void Update ()
    {
        if (waiting == true)
        {
            PlayerMovement.playerEnabled = false;
            WaitMessage();
        }
        if (timer <= 0)
        {
            PlayerMovement.playerEnabled = true;
            waitValue.gameObject.SetActive(false);
            waitText.SetActive(false);
            waiting = false;
        }
	}

    void WaitMessage()
    {
        waitValue.gameObject.SetActive(true);
        waitValue.text = timer.ToString();
        timer -= Time.deltaTime;
        waitText.SetActive(true);
    }
}
