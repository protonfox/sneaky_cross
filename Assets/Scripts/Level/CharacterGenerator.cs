﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterGenerator : MonoBehaviour
{
    [SerializeField]
    bool spawn, spawnPursuer;

    [SerializeField]
    GameObject NPC;

    [SerializeField]
    int maxNPC, maxPursuer;

    [SerializeField]
    List <GameObject> spawnPoints =  new List<GameObject>();

    GameObject pursuerHat, pursuerShirt, pursuerPants, pursuerShoes, randomNPC, crowd, randomPos;
    GameObject pursuerHat2, pursuerShirt2, pursuerPants2, pursuerShoes2;
    GameObject pursuerHat3, pursuerShirt3, pursuerPants3, pursuerShoes3;
    Renderer[] npcParts;
    public static Vector3 randomSpawn;
    public static int spawnCount, spawnPointCount, npcs, pursuerCount;

    void Start()
    {
        pursuerCount = 0;
        crowd = GameObject.Find("Crowd");
        Randomizer1();
        Randomizer2();
        Randomizer3();
    }

    void Update()
    {
        npcs = GameObject.FindGameObjectsWithTag("NPC").Length;
        randomPos = spawnPoints[spawnPointCount];
        randomSpawn = randomPos.transform.position;

        if (maxPursuer == 0)
        {
            spawnPursuer = false;
        }

        if (pursuerCount != maxPursuer)
        {
            spawnPursuer = true;
            pursuerCount++;
        }


        if (spawnPointCount == spawnPoints.Count - 1)
        {
            spawnPointCount = 0;
        }

        if (npcs < maxNPC)
        {
            spawn = true;
        }

        if (spawn == true)
        {
            SpawnNPC();
            spawnCount++;
            spawnPointCount++;

            if (spawnCount >= maxNPC)
            {
                spawn = false;
            }
        }
        PursuerSpawn();

    }

    void PursuerSpawn()
    {
        if (spawnPursuer == true)
        {
            SpawnNPC();
            randomNPC.name = "Pursuer" + pursuerCount;

            Color hat = npcParts[2].material.color;
            Color shirt = npcParts[3].material.color;
            Color pants = npcParts[5].material.color;
            Color shoes = npcParts[7].material.color;

            if (pursuerCount == 1)
            {
                pursuerHat.GetComponent<SpriteRenderer>().material.SetColor("_Color", hat);
                pursuerShirt.GetComponent<SpriteRenderer>().material.SetColor("_Color", shirt);
                pursuerPants.GetComponent<SpriteRenderer>().material.SetColor("_Color", pants);
                pursuerShoes.GetComponent<SpriteRenderer>().material.SetColor("_Color", shoes);
                pursuerHat.SetActive(false);
                pursuerShirt.SetActive(false);
                pursuerPants.SetActive(false);
                pursuerShoes.SetActive(false);
            }
            if (pursuerCount == 2)
            {
                if (GameObject.Find("Randomizer 2"))
                {
                    pursuerHat2.GetComponent<SpriteRenderer>().material.SetColor("_Color", hat);
                    pursuerShirt2.GetComponent<SpriteRenderer>().material.SetColor("_Color", shirt);
                    pursuerPants2.GetComponent<SpriteRenderer>().material.SetColor("_Color", pants);
                    pursuerShoes2.GetComponent<SpriteRenderer>().material.SetColor("_Color", shoes);
                    pursuerHat2.SetActive(false);
                    pursuerShirt2.SetActive(false);
                    pursuerPants2.SetActive(false);
                    pursuerShoes2.SetActive(false);
                }
            }
            if (pursuerCount == 3)
            {
                if (GameObject.Find("Randomizer 3"))
                {
                    pursuerHat3.GetComponent<SpriteRenderer>().material.SetColor("_Color", hat);
                    pursuerShirt3.GetComponent<SpriteRenderer>().material.SetColor("_Color", shirt);
                    pursuerPants3.GetComponent<SpriteRenderer>().material.SetColor("_Color", pants);
                    pursuerShoes3.GetComponent<SpriteRenderer>().material.SetColor("_Color", shoes);
                    pursuerHat3.SetActive(false);
                    pursuerShirt3.SetActive(false);
                    pursuerPants3.SetActive(false);
                    pursuerShoes3.SetActive(false);
                }
            }

            spawnPursuer = false;
        }
    }

    void SpawnNPC()
    {
        randomNPC = Instantiate(NPC, randomSpawn, Quaternion.identity, crowd.transform);

        npcParts = randomNPC.GetComponentsInChildren<Renderer>();
        npcParts[1].material.SetColor("_Color", Random.ColorHSV()); // Hat Tip
        npcParts[2].material.SetColor("_Color", npcParts[1].material.color); // Hat
        npcParts[3].material.SetColor("_Color", Random.ColorHSV()); // Shirt
        npcParts[4].material.SetColor("_Color", Random.ColorHSV()); // Pants
        npcParts[5].material.SetColor("_Color", npcParts[4].material.color); // Pants
        npcParts[6].material.SetColor("_Color", Random.ColorHSV()); // Shoe
        npcParts[7].material.SetColor("_Color", npcParts[6].material.color); // Shoe
    }

    void Randomizer1()
    {
        pursuerHat = GameObject.Find("Pursuer Hat Colour1");
        pursuerShirt = GameObject.Find("Pursuer Shirt Colour1");
        pursuerPants = GameObject.Find("Pursuer Pants Colour1");
        pursuerShoes = GameObject.Find("Pursuer Shoes Colour1");
    }
    void Randomizer2()
    {
        if (GameObject.Find("Randomizer 2"))
        {
            pursuerHat2 = GameObject.Find("Pursuer Hat Colour2");
            pursuerShirt2 = GameObject.Find("Pursuer Shirt Colour2");
            pursuerPants2 = GameObject.Find("Pursuer Pants Colour2");
            pursuerShoes2 = GameObject.Find("Pursuer Shoes Colour2");
        }
    }
    void Randomizer3()
    {
        if (GameObject.Find("Randomizer 3"))
        {
            pursuerHat3 = GameObject.Find("Pursuer Hat Colour3");
            pursuerShirt3 = GameObject.Find("Pursuer Shirt Colour3");
            pursuerPants3 = GameObject.Find("Pursuer Pants Colour3");
            pursuerShoes3 = GameObject.Find("Pursuer Shoes Colour3");
        }
    }
}
