﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ticket : MonoBehaviour
{
    [SerializeField]
    GameObject barrier;

    GameObject ticketShow;

    void Start ()
    {
        ticketShow = GameObject.Find("Ticket");
        if (ticketShow)
        {
            ticketShow.SetActive(false);
        }
    }

    void Update ()
    {
        TicketDisplay();
	}

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (PlayerMovement.ticket > 0)
            {
                PlayerMovement.ticket = 0;
                Object.Destroy(barrier);
                ticketShow.SetActive(true);
            }
        }
    }

    void TicketDisplay()
    {
        if (PlayerMovement.ticket > 0)
        {
            if (ticketShow)
            {
                ticketShow.SetActive(true);
            }
        }
    }
}
