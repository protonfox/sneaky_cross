﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockHints : MonoBehaviour
{
    [SerializeField]
    float cameraTime, hatTime, shirtTime, pantsTime, shoesTime, resetTimer;

    public static float unlockCameraBuff;

    [SerializeField]
    GameObject hat, shirt, pants, shoes;
    float timer;

    void Start ()
    {
        unlockCameraBuff = cameraTime;
        //hat = GameObject.Find("Pursuer Hat Colour");
        //shirt = GameObject.Find("Pursuer Shirt Colour");
        //pants = GameObject.Find("Pursuer Pants Colour");
        //shoes = GameObject.Find("Pursuer Shoes Colour");
    }

    void Update ()
    {
        timer += Time.deltaTime;

        if (timer > 1)
        {
            hat.SetActive(false);
            shirt.SetActive(false);
            pants.SetActive(false);
            shoes.SetActive(false);
        }

        if (timer > hatTime)
        {
            UnlockHat();
        }

        if (timer > shirtTime)
        {
            UnlockShirt();
        }

        if (timer > pantsTime)
        {
            UnlockPants();
        }

        if (timer > shoesTime)
        {
            UnlockShoes();
        }

        if (timer > resetTimer)
        {
            timer = 0;
        }
    }
    
    void UnlockHat()
    {
        hat.SetActive(true);
        shirt.SetActive(false);
        pants.SetActive(false);
        shoes.SetActive(false);
    }

    void UnlockShirt()
    {
        hat.SetActive(false);
        shirt.SetActive(true);
        pants.SetActive(false);
        shoes.SetActive(false);
    }

    void UnlockPants()
    {
        hat.SetActive(false);
        shirt.SetActive(false);
        pants.SetActive(true);
        shoes.SetActive(false);
    }

    void UnlockShoes()
    {
        hat.SetActive(false);
        shirt.SetActive(false);
        pants.SetActive(false);
        shoes.SetActive(true);
    }
}
