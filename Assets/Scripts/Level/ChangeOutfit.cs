﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeOutfit : MonoBehaviour
{
    [SerializeField]
    Material playerHead, playerShirt, playerPants, playerShoes;

    [SerializeField]
    Button randomButton;

    string head, shirt, pants, shoes;

    void Start()
    {

    }

    void Update()
    {
        //CurrentOutfit();

        randomButton.onClick.AddListener(Randomize);
    }

    void Randomize()
    {
        playerHead.SetColor("_Color", Random.ColorHSV());
        playerShirt.SetColor("_Color", Random.ColorHSV());
        playerPants.SetColor("_Color", Random.ColorHSV());
        playerShoes.SetColor("_Color", Random.ColorHSV());
        //PlayerPrefs.SetString("Head", head);
        //PlayerPrefs.SetString("Shirt", shirt);
        //PlayerPrefs.SetString("Pants", pants);
        //PlayerPrefs.SetString("Shoes", shoes);

    }

    void CurrentOutfit()
    {
        head = playerHead.color.ToString();
        shirt = playerShirt.color.ToString();
        pants = playerPants.color.ToString();
        shoes = playerShoes.color.ToString();
    }
}
