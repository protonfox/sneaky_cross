﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomizeOutfit : MonoBehaviour
{
    Renderer[] npcParts;

    void Start ()
    {
        npcParts = this.GetComponentsInChildren<Renderer>();
        npcParts[1].material.SetColor("_Color", Random.ColorHSV()); // Hat Tip
        npcParts[2].material.SetColor("_Color", npcParts[1].material.color); // Hat
        npcParts[3].material.SetColor("_Color", Random.ColorHSV()); // Shirt
        npcParts[4].material.SetColor("_Color", Random.ColorHSV()); // Pants
        npcParts[5].material.SetColor("_Color", npcParts[4].material.color); // Pants
        npcParts[6].material.SetColor("_Color", Random.ColorHSV()); // Shoe
        npcParts[7].material.SetColor("_Color", npcParts[6].material.color); // Shoe
    }
	
	void Update ()
    {
		
	}
}
