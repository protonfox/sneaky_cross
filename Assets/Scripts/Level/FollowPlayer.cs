﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    [SerializeField]
    bool follow, lookAt;

    [SerializeField]
    Vector3 above = new Vector3(0f, 2f, -8f);



    float distance = 10f;
    Transform pursuer;
    float timer;

    void FixedUpdate() // Updates the camera's position based on where the player is and elevates above it.
    {
        timer += Time.deltaTime;
        Follow();
        LookAt();

        if (timer > 3)
        {
            pursuer = GameObject.Find("Pursuer1").transform;
        }
        if (timer > UnlockHints.unlockCameraBuff)
        {
            lookAt = true;
        }
    }

    void Follow()
    {
        if (follow == true)
        {
            Vector3 goTo = pursuer.position + above;
            Vector3 currentPos = Vector3.Lerp(transform.position, goTo, distance * Time.deltaTime);
            transform.position = currentPos;
        }
    }

    void LookAt()
    {
        if (lookAt == true)
        {
            transform.LookAt(pursuer.position);
        }
    }
}
