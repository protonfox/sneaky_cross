﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAndRotate : MonoBehaviour
{
    [SerializeField]
    Vector3 movementVector = new Vector3(0.0f, 0.0f, 0.0f);

    [SerializeField]
    Vector3 rotationVector = new Vector3(0.0f, 0.0f, 0.0f);

    float tick;

    [SerializeField]
    float scale = 1f;

    [SerializeField]
    bool useTick = false, useScale = false;

    void FixedUpdate()
    {
        if (Time.timeScale == 0) return;
        tick = Time.deltaTime;
        Move();
        Rotate();
        Scale();
    }

    public void Move() // Used to move the object in any direction constantly.
    {
        Vector3 movingVector = movementVector;
        if (useTick == true)
        {
            movingVector = movingVector * tick;
            transform.Translate(movementVector, Space.World);
        }
        transform.Translate(movementVector, Space.World);
    }

    public void Rotate() // Used to rotate an object constantly.
    {
        Vector3 rotatingVector = rotationVector;
        if (useTick == true)
        {
            rotatingVector = rotatingVector * tick;
            transform.Rotate(rotationVector, Space.World);
        }
        transform.Rotate(rotationVector, Space.World);
    }

    public void Scale() // Used to manually change the scale of the entire object.
    {
        if (useScale == true)
        {
            transform.localScale = new Vector3(scale, scale, scale);
        }
    }
}
