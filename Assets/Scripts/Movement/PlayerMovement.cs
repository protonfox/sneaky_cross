﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    float defaultSpeed, sneakSpeed;

    [SerializeField]
    GameObject radius, cameraMain, cameraRandom;

    [SerializeField]
    GameObject hat, paper;

    public static bool playerEnabled;

    Rigidbody rig;
    string axisX = "Horizontal";
    string axisZ = "Vertical";
    float moveX, moveZ;
    float speed;
    Renderer hatColour;
    public static int ticket;

    void Start ()
    {
        rig = this.GetComponent<Rigidbody>();
        if (rig == null)
        {
            Debug.LogError("Did you add a rigidbody?");
        }
        speed = defaultSpeed;
        playerEnabled = true;
    }

    void Update ()
    {
        moveX = Input.GetAxis(axisX);
        moveZ = Input.GetAxis(axisZ);

        hatColour = hat.GetComponentInChildren<Renderer>();
    }

    void FixedUpdate()
    {
        if (playerEnabled == true)
        {
            Vector3 moveVector = transform.forward * moveZ * (speed * Time.deltaTime);
            Vector3 rotateVector = new Vector3(0, moveX, 0) * speed;
            this.transform.Translate(moveVector, Space.World);
            this.transform.Rotate(rotateVector, Space.World);

            Sneaking();
            Focus();
        }
    }

    void Sneaking()
    {
        //GameObject pursuer = GameObject.Find("Pursuer");
        //Collider pursuerCol = pursuer.GetComponent<Collider>();
        BoxCollider col = radius.GetComponent<BoxCollider>();

        if (Input.GetButtonDown("Sneak"))
        {
            col.size = new Vector3(0.1f, 3, 0.1f);
            speed = sneakSpeed;

            if (LoadScenes.sceneName == "Stage1Level1" || LoadScenes.sceneName == "Stage1Level2" || LoadScenes.sceneName == "Stage1Level3")
            {
                hatColour.sharedMaterial.SetColor("_Color", Random.ColorHSV());
                hat.SetActive(true);
                //pursuerCol.transform.localScale =  new Vector3(0,0,0);
            }
            if (LoadScenes.sceneName == "Stage2Level1" || LoadScenes.sceneName == "Stage2Level2" || LoadScenes.sceneName == "Stage2Level3")
            {
                paper.SetActive(true);
            }
        }

        if (Input.GetButtonUp("Sneak"))
        {
            speed = defaultSpeed;
            hat.SetActive(false);
            paper.SetActive(false);
            col.size = new Vector3(6, 6, 6);
            //pursuerCol.transform.localScale = new Vector3(1, 3.916261f, 1);
        }
    }

    void Focus()
    {
        if (Input.GetButtonDown("Focus"))
        {
            cameraRandom.SetActive(true);
            cameraMain.SetActive(false);
        }
        if (Input.GetButtonUp("Focus"))
        {
            cameraMain.SetActive(true);
            cameraRandom.SetActive(false);
        }
    }
    
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name == "Pursuer1" || other.gameObject.name == "Pursuer2" || other.gameObject.name == "Pursuer3")
        {
            Object.Destroy(gameObject);
            // Have the round end.
        }
        if (other.gameObject.name == "Machine")
        {
            ticket++;
        }
    }
}
