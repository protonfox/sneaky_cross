﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PursuerMovement : MonoBehaviour
{
    GameObject despawn, player;
    Rigidbody rig;

    void Update()
    {
        despawn = GameObject.Find("Despawn Zone");
        player = GameObject.Find("Player");
        rig = gameObject.GetComponent<Rigidbody>();
        PlayerCaught();
    }

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player Radius")
        {
            if (this.gameObject.name == "Pursuer1" || this.gameObject.name == "Pursuer2" || this.gameObject.name == "Pursuer3")
            {
                transform.LookAt(2 * transform.position - other.transform.parent.position);
                // Run Towards Player
            }
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player Radius")
        {
            if (this.gameObject.name == "Pursuer1" || this.gameObject.name == "Pursuer2" || this.gameObject.name == "Pursuer3")
            {
                transform.LookAt(2 * transform.position - despawn.transform.position);
                transform.rotation = Quaternion.identity;
            }
        }
    }
    
    void PlayerCaught()
    {
        if (!player)
        {
            transform.LookAt(2 * transform.position - despawn.transform.position);
            transform.rotation = Quaternion.identity;
        }
    }
}
