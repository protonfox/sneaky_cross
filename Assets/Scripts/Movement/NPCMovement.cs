﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCMovement : MonoBehaviour
{
    float randomSpeed;
    Transform spawn;

	void Start ()
    {
        randomSpeed = Random.Range(2, 6);
        spawn = GameObject.Find("Spawn Zone").transform;
    
    }

    void Update ()
    {
        transform.Translate(-transform.forward * Time.deltaTime * randomSpeed);

	}

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Despawn")
        {
            if (CharacterGenerator.spawnPointCount == 8)
            {
                CharacterGenerator.spawnPointCount = 0;
            }
            //Object.Destroy(gameObject);
            CharacterGenerator.spawnPointCount++;
            transform.position = CharacterGenerator.randomSpawn;
        }
        if (this.name != "Pursuer1")
        {
            if (other.transform.tag == "Destroy")
            {
                Object.Destroy(gameObject);
            }
        }
        if (this.name == "Pursuer1")
        {
            if (other.transform.tag == "Destroy")
            {
                if (CharacterGenerator.spawnPointCount == 8)
                {
                    CharacterGenerator.spawnPointCount = 0;
                }
                //Object.Destroy(gameObject);
                CharacterGenerator.spawnPointCount++;
                transform.position = CharacterGenerator.randomSpawn;
            }
        }
    }
}
