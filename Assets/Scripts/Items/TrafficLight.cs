﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficLight : MonoBehaviour
{
    [SerializeField]
    Renderer redLight, amberLight, greenLight;

    [SerializeField]
    bool red, amber, green;

	void Start ()
    {

    }
	
    void FixedUpdate()
    {
        TurnOn();
        TurnOff();
    }

	void TurnOn()
    {
        if (red == true)
        {
            redLight.material.SetColor("_Color", Color.red);
            redLight.material.SetColor("_SpecColor", Color.red);
        }
        if (amber == true)
        {
            amberLight.material.SetColor("_Color", Color.yellow);
            amberLight.material.SetColor("_SpecColor", Color.red);
        }
        if (green == true)
        {
            greenLight.material.SetColor("_Color", Color.green);
            greenLight.material.SetColor("_SpecColor", Color.green);
        }
	}

    void TurnOff()
    {
        if (red == false)
        {
            redLight.material.SetColor("_Color", Color.white);
            redLight.material.SetColor("_SpecColor", Color.white);
        }
        if (amber == false)
        {
            amberLight.material.SetColor("_Color", Color.white);
            amberLight.material.SetColor("_SpecColor", Color.white);
        }
        if (green == false)
        { 
            greenLight.material.SetColor("_Color", Color.white);
            greenLight.material.SetColor("_SpecColor", Color.white);
        }
    }
}
