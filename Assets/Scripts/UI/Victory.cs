﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Victory : MonoBehaviour
{
    [SerializeField]
    GameObject victoryCanvas;

    [SerializeField]
    AudioClip goalReached; 

    public static bool levelFinished;

    void Start() // Prevents the canvas from showing when started.
    {
        levelFinished = false;
        victoryCanvas.SetActive(false);
    }

    void Update()
    {

    }
    void OnTriggerEnter(Collider other) // When the win flag is triggered; display the victory screen, play a sound and write stats.
    {
        if (other.tag == "Player")
        {
            Time.timeScale = 0;
            levelFinished = true;
            victoryCanvas.SetActive(true);
            //AudioSource.PlayClipAtPoint(goalReached, transform.position);
            //Debug.Log("Wrote stats");
            //WriteStats.WriteString();
        }
    }
}
