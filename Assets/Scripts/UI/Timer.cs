﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    // Answers.unity.com. 2018. Making A Timer (00:00) Minutes And Seconds - Unity Answers. [online] Available at: <https://answers.unity.com/questions/45676/making-a-timer-0000-minutes-and-seconds.html> [Accessed 29 April 2018].

    [SerializeField]
    TextMesh timeObject;

    public static int minutes, seconds;
    public static string total;
    
	void Update () // Records time since the level has loaded and stores it as a string.
    {
		minutes = Mathf.FloorToInt(Time.timeSinceLevelLoad / 60);
        seconds = Mathf.RoundToInt(Time.timeSinceLevelLoad % 60);
        total = string.Format("{0:00}:{1:00}", minutes, seconds);
        TimeText();
    }

    void TimeText()
    {
        timeObject.text = total;
    }
}
