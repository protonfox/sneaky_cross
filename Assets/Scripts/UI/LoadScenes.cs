﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScenes : MonoBehaviour
{
    // Answers.unity.com. 2018. Saving An Int Via Playerprefs... - Unity Answers. [online] Available at: <https://answers.unity.com/questions/473643/saving-an-int-via-playerprefs.html> [Accessed 29 April 2018].

    [SerializeField]
    int loadScene;

    public static string levelsUnlocked, sceneName;
    Scene scene;

    void Update()
    {
        SceneName();
    }

    public void Load(int level) // When the level is loaded; reset the lives, score, teleport count and load the level.
    {
        //AddToScore.score = 0;
        Time.timeScale = 1;
        //Pause.gamePaused = false;
        AudioListener.pause = false;
        SceneManager.LoadScene(level);

    }

    public void ExitGame() // Simply quits the game when pressed.
    {
        Application.Quit();
    }

    internal static int Load(object level)
    {
        throw new NotImplementedException();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            SceneManager.LoadScene(loadScene);
        }
    }

    public void SceneName()
    {
        scene = SceneManager.GetActiveScene();
        sceneName = scene.name;
    }
}
