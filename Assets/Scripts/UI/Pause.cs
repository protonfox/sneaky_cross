﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    // Murray, J., 2018. Quick Tip: Pause, Slow Motion & Double-Time In Unity. [online] Code Envato Tuts+. Available at: <https://code.tutsplus.com/articles/quick-tip-pause-slow-motion-double-time-in-unity--active-8015> [Accessed 29 April 2018].

    [SerializeField]
    GameObject pauseCanvas;

    public static bool gamePaused = false;

    void Start() // Makes sure the canvas is disabled at the start of the level.
    {
        pauseCanvas.SetActive(false);
    }
	void Update()
    {
        if (/*Victory.levelFinished == false && */ gamePaused == false) // If the game isn't paused, pressing Esc pauses the game.
        {
            if (Input.GetButtonDown("Esc"))
            {
                Time.timeScale = 0;
                pauseCanvas.SetActive(true);
                gamePaused = true;
                AudioListener.pause = true;
                //AutoRun.enable = false;
            }
        }
        else if (gamePaused == true)
        {
            if (Input.GetButtonDown("Esc")) // If the game is paused, pressing Esc unpauses the game.
            {
                UnPause();
            }
        }
    }
    public void UnPause() // Resumes the game and disables the canvas.
    {
        Time.timeScale = 1;
        pauseCanvas.SetActive(false);
        gamePaused = false;
        AudioListener.pause = false;
        //AutoRun.enable = true;
    }
}
