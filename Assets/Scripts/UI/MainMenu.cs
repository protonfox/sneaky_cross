﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    List<Button> buttons = new List<Button>();

    int buttonCount;

	void Start ()
    {
		
	}

    public void LeftButton()
    {
        if (buttonCount > 0)
        {
            buttons[buttonCount].gameObject.SetActive(false);
            buttonCount--;
            buttons[buttonCount].gameObject.SetActive(true);
        }
    }

    public void RightButton()
    {
        if (buttonCount != buttons.Count -1)
        {
            buttons[buttonCount].gameObject.SetActive(false);
            buttonCount++;
            buttons[buttonCount].gameObject.SetActive(true);
        }
    }
}
