﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour
{
    [SerializeField]
    GameObject gameOverCanvas;

    GameObject player, cam;
    public static bool gameOver;

	void Start ()
    {
        gameOver = false;
        cam = GameObject.Find("Death Cam");
        player = GameObject.Find("Player");

        gameOverCanvas.SetActive(false);
        cam.SetActive(false);
	}
	
	void Update ()
    {
		if (!player)
        {
            ShowGameOverCanvas();
        }
	}

    void ShowGameOverCanvas()
    {
        gameOverCanvas.SetActive(true);
        gameOver = true;
        cam.SetActive(true);
    }
}
